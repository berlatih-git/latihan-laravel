<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulir Pendaftaran</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <form action="kirim" method="POST">
        @csrf
        <h3>Sign Up Form</h3>
        <p>First Name:</p>
            <input type="text" name="nama1">
        <p>Last Name:</p>
            <input type="text" name="nama2">
        <p>Gender:</p>
            <input type="radio" id="man" name="gender" value="Male">
                <label for="man">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">    
                <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="Other">
                <label for="other">Other</label>
        <p>Nationality</p>
            <select>
                <option value="1">Indonesia</option>
                <option value="2">Arabian</option>
                <option value="3">Korean</option>
            </select>
        <p>Language Spoken:</p>
            <input type="checkbox">Bahasa Indonesia
            <br>
            <input type="checkbox">English
            <br>
            <input type="checkbox">Other
        <p>Bio:</p>
            <textarea name="" id="" cols="30" rows="10"></textarea>
            <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>