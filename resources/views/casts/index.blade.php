@extends('master')

@section('content')
<div>

    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Data Casts</h3>
        </div>
        <!-- /.card-header -->
        
        <div class="card-body">
            @if(session('Success'))
                <div class="alert alert-success">
                    {{session('Success')}}
                </div>                   
            @endif
            <div class="mb-2">
                <a class="btn btn-primary" href="/cast/create">Buat Data Baru !</a>
            </div>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 5%">ID</th>
                <th style="width: 40%">Nama</th>
                <th style="width: 7%">Umur</th>
                <th style="width: 40%">Biografi</th>
                <th style="width: 8%">Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach($casts as $key => $cast)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $cast->nama}} </td>
                    <td> {{ $cast->umur}} </td>
                    <td> {{ $cast->bio}} </td>
                    <td style="display : flex;">
                        <a href="/cast/{{$cast->id}}" class="btn btn-info">Show</a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-secondary ml-2">Edit</a>
                        <form action="/cast/{{$cast->id}}" method="post">
                        @csrf
                        @method('DELETE')
                            <input type="submit" value="Delete" class="btn btn-danger ml-2">
                        </form>                        
                    </td>
            </tr>
                @endforeach

            </tbody>
        </table>
        </div>
        <!-- /.card-body -->
    </div>
</div>
@endsection