@extends('master')

@section('content')
<div class="ml-2 mr-2 mt-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Cast !</h3>
        </div>
        <form role="form" action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="enterNama">Name</label>
                    <input type="text" class="form-control" name ="nama" id="enterNama" placeholder="Your name" value=" {{old('nama', $cast->nama)}} ">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="inputUmur">Age</label>
                    <input type="text" class="form-control" name="umur" id="inputUmur" placeholder="Your Age" value=" {{old('nama', $cast->umur)}} ">
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="inputBio">Your Biography</label>
                    <input type="text" id="inputBio" name="bio" class="form-control" rows="3" placeholder="Enter ..." value=" {{old('bio', $cast->bio)}} "></input>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->
    
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>

</div>
@endsection