<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('form');
    }

    public function kirim(Request $request){
        $nama = $request['nama1'];
        $namaBelakang = $request['nama2'];
        return view('welcome', compact('nama','namaBelakang'));

    }
}
